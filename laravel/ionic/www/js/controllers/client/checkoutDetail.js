angular.module('starter.controllers')
    .controller('ClienteCheckoutDetailCtrl',[
        '$scope', '$state','$stateParams','$cart', function($scope, $state, $stateParams, $cart){

            $scope.product = $cart.getItem($stateParams.index);

            $scope.somenteNumero = function(){
                var val = $scope.product.qtd;
                console.log($cart.getItem($stateParams.index));
                $scope.product.qtd = parseInt(val.toString().replace(/[^0-9]/g, ''));
                if($scope.product.qtd > 10){
                    $scope.product.qtd = 10;
                }
                if(!$scope.product.qtd){
                    $scope.product.qtd = "";
                }
            };



            //$('#txtValor').on('keyup', function(){
            //    var maximo = 10;
            //    if(event.target.value > maximo){
            //        return event.target.value = maximo;
            //    }
            //    if($scope.product > event.target.value){
            //        return $scope.product = event.target.value;
            //    }
            //});

            $scope.updateQtd = function(){
                $cart.updateQtd($stateParams.index, $scope.product.qtd);
                $state.go('client.checkout')
            };

            $scope.openCheckout = function(){
                $state.go('client.checkout')
            };

        }]);