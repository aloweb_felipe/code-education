<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Models\User;

/**
 * Class UserTransformer
 * @package namespace App\Transformers;
 */
class UserTransformer extends TransformerAbstract
{

    /**
     * Transform the \User entity
     * @param \User $model
     *
     * @return array
     */
    public function transform(User $model)
    {
        return [
            'id'         => (int) $model->id,
            'name' => $model->name,
            'email'=> $model->email,
            'role' => $model->role,
        ];
    }

    public function includeClient(User $model){
        if($model->client){
            return $this->item($model->client, new ClientTransformer());
        }else{
            return null;
        }

    }
}
