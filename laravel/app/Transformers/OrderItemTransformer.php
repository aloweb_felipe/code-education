<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Models\OrderItem;

/**
 * Class OrderItemTransformer
 * @package namespace App\Transformers;
 */
class OrderItemTransformer extends TransformerAbstract
{

    /**
     * Transform the \OrderItem entity
     * @param \OrderItem $model
     *
     * @return array
     */
    public function transform(OrderItem $model)
    {
        return [
            'id'         => (int) $model->id,
            'product_id' => (int) $model->product_id,
            'qtd'        => $model->qtd,
            'price'      => $model->price,
            'created_at' => $model->created_at,
            'updated_at' => $model->updated_at
        ];
    }
}
