<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface CupomRepository
 * @package namespace CodeDelivery\Repositories;
 */
interface CupomRepository extends RepositoryInterface
{
    public function findByCode($code);
}
