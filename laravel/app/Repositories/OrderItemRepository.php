<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface OrderItemRepository
 * @package namespace CodeDelivery\Repositories;
 */
interface OrderItemRepository extends RepositoryInterface
{
    //
}
