<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface CategoryRepository
 * @package namespace CodeDelivery\Repositories;
 */
interface CategoryRepository extends RepositoryInterface
{

}
