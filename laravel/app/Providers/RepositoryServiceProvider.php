<?php
namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{

    public function boot() {}

    public function register()
    {
        $models = array(
            'Category',
            'Client',
            'OrderItem',
            'Order',
            'User',
            'Product',
            'Cupom',
        );

        foreach($models as $model) {
        $this->app->bind(
            "App\\Repositories\\{$model}Repository",
            "App\\Repositories\\{$model}RepositoryEloquent"
        );}
    }
}
